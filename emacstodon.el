;;; emacstodon.el --- Emacs Mode for Mastodon -*- lexical-binding: t; -*-

;; Copyright (C) 2020 MaterialFuture

;; Author: @MaterialFuture <admin@andrasmedia.org>
;; Maintainer: @MaterialFuture <admin@andrasmedia.org>
;; Created: April 26, 2020
;; Modified: April 26, 2020
;; Version: 0.0.1
;; Keywords: mastodon, toot, toots
;; Homepage: https://github.com/materialfuture/emacstodon
;; Package-Requires: ((emacs 26.3) (cl-lib "0.5"))

;; This file is not part of GNU Emacs.

;;; Commentary:

;; A simple lightweight mastodon mode for GNU Emacs.
;; Show Home Timeline

;;; Feature Requests

;; Show Local Timeline
;; Show Federated Timeline
;; Compose Toot
;; See Profiles

;;; Code:
(require 'url)
(require 'json)
(require 'cl-lib)
(require 'request)
(require 'url-parse)
(require 'url-queue)

(defgroup mast nil
  "Customization group for the `emacstodon' package."
  :prefix "mast-"
  :tag "Mast"
  :group 'applications)

(defconst mast-version ()
  "Print and return the version of the `sx' package."
  (interactive)
  (message "%s: %s" 'sx-version sx-version)
  sx-version)

;; Breakdown the request
;; (defconst mastodon-api-protocol "https")
;; (defconst mastodon-api-uri "api/v1/")
;; (defconst mastodon-instance-uri "merveilles.town/")
;; (defvar   mastodon-request-url
;;           (cons mastodon-api-uri
;;                 mastodon-instance-uri))
;; (insert mastodon-request-url)

(defcustom mast-curl-timeout 30
  "The timeout for the curl request."
  :type  'integer)

(defcustom mst-curl-max-toots 20
  "The max ammount of toots for the curl request."
  :type  'integer)

(defvar request-data)
(request
 "https://merveilles.town/api/v1/timelines/public"
 :params '(("local" . "true") ("limit" . "5"))
 :parser 'json-read
 :success (cl-function
           (lambda (&key data &allow-other-keys)
             (message "I get: %S" (assoc-default 'args data))
             (setq request-data data)
             (print "SUCCESS")))
 :error (cl-function
           (lambda (&key data &allow-other-keys)
             (message "There was an ERROR getting your data")
             (print "ERROR"))))

(provide 'emacstodon)
;;; emacstodon.el ends here
